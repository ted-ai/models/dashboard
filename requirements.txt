boto3==1.28.65
gradio==3.49.0
pandas==2.0.3
requests==2.31.0
urllib3==1.26.18
